<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $pages=\App\Page::get();
    return view('index',['pages'=>$pages]);
    //return view('welcome');
});



Route::get('/about123', function () {

    return view('about');
});
Route::post('signup','AuthController@signUp')->name('signup');
Route::post('signin','AuthController@signIn')->name('signin');
Route::get('activate/account/{email}','AuthController@activateAccount')->name('activateAccount');
Route::post('forgot/password/','AuthController@forgotPassword')->name('forgotPassword');
Route::get('reset/password/{email}','AuthController@resetPassword')->name('resetPassword');



Route::group(['middleware' => 'auth'], function () {
    Route::get('dashboard','UserController@userDashboard')->name('userDashboard');
});


Route::group(['namespace' => 'admin', // it is added in controller function name
    'prefix' => 'admin', //it is added in url
    'as' => 'admin.' //route name
], function () {

Route::get('','AdminController@index')->name('admin');

Route::post('/login','AdminController@login')->name('adminLogin');

Route::get('/dashboard','AdminController@dashboard')->name('dashboard');
Route::get('/Pages','PageController@pages')->name('pages');
Route::get('/add/page','PageController@addPage')->name('addPage');
Route::post('/store/page','PageController@storePage')->name('storePage');
Route::get('/edit/{id}/page','PageController@editPage')->name('editPage');
Route::post('/update/page','PageController@updatePage')->name('updatePage');
Route::get('/view/{id}/page','PageController@viewPage')->name('viewPage');

Route::get('/media','MediaController@index')->name('media');
Route::get('/add/media','MediaController@addMedia')->name('addMedia');
Route::post('/store/media','MediaController@storeMedia')->name('storeMedia');
Route::get('/edit/{id}/media','MediaController@editMedia')->name('editMedia');
Route::post('/update/media','MediaController@updateMedia')->name('updateMedia');
Route::get('/view/{id}/media','MediaController@viewMedia')->name('viewMedia');

});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

//Route::get('{slug}', ['uses' => 'PageController@getPage'])->where('slug', '([A-Za-z0-9\-\/]+)');
//Route::get('/{slug}', array('as' => 'page.show', 'uses' => 'PageController@getPage'));

Route::get('aboutus','AboutusController@index')->name('aboutus');