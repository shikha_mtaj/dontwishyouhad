<?php
use App\Page;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Don't wish you had</title>

    <link rel="icon" href="{{asset('assets/dist/img/favicon.ico')}}" type="image/x-icon">

    <link href="{{asset('assets/frontend/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/css/font-awesome.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/css/developer.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/css/loadme.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/css/responsive.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/sweetalert2/sweetalert2.css') }}">

    <script src="{{asset('assets/sweetalert2/sweetalert2.js') }}"></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    </head>
    <body>
    <header>
        <div class="container">
            <div class="row">

                <div class="col-md-1 col-sm-2 col-xs-12">
                    <div class="logo">

                        <a href="{{ url('/') }}"><img src="{{asset('assets/frontend/images/logo.png')}}" class="img-responsive"  alt="Logo"></a>

                    </div>
                    <div class="navbar-header resp-div">
                        <button type="button" class="navbar-toggle collapsed resp-btn" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <div class="clearfix"></div>

                </div>

                <div class="col-md-11 col-sm-10 col-xs-12">
                    <nav class="navbar navbar-default menubar">
                        <div class="container-fluid mbar">
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav menulinks1">

                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Company <span class="caret"></span></a>
                                        <ul class="dropdown-menu dmenu">
                                            <li><a href="#">About Don't Wish You Had</a></li>
                                            <li><a href="#">In The Media</a></li>
                                            <li><a href="#">Testimonials</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Info<span class="caret"></span></a>
                                        <ul class="dropdown-menu dmenu">
                                            <li><a href="#">FAQ</a></li>
                                            <li><a href="#">Get Inspired</a></li>
                                            <li><a href="#">Message Types</a></li>
                                            <li><a href="#">Public Messages</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Customer care <span class="caret"></span></a>
                                        <ul class="dropdown-menu dmenu">
                                            <li><a href="#">Contact Us</a></li>
                                            <li><a href="#">Plans</a></li>
                                            <li><a href="#">Terms of Service</a></li>
                                            <li><a href="#">Privacy Policy</a></li>
                                            <li><a href="#">Report User's Death</a></li>
                                        </ul>
                                    </li>
                                    {{--<li><a href="{{ route('aboutus') }}">About Us</a></li>--}}
                                    <?php /* $pages = Page::get(); ?>
                                    @foreach($pages as $page)
                                        <li><a href="{{$page->slug}}">{{$page->name}}</a></li>
                                    @endforeach
                                    <?php */ ?>
                                </ul>
                                <ul class="nav navbar-nav navbar-right menulinks2">
                                    <li><a href="javascript:void(0)">Invite</a></li>
                                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#signupModal">Sign up</a></li>
                                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#loginModal">Login</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="clearfix"></div>

            </div>
        </div>

    </header>
