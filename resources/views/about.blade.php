@extends('layouts.frontmain')

@section('content')

    <section class="banner-wrap">
        <img src="{{asset('images/cms/'.$pagecontent->banner_image)}}" alt="">
        <div class="heading">
            <h1>{{ $pagecontent->title }}</h1>
        </div>
        <div class="slantdiv1 hidden-xs"></div>
    </section>

    <section class="about_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="abt_box">
                        <div class="heading">
                            <h4>Pellentesque vel dolor</h4>
                        </div>
                        <p>Quisque tincidunt velit non dolor tristique consectetur. Donec nec dapibus velit, vitae aliquet velit. Quisque fermentum sed augue et vulputate. Morbi vestibulum erat et metus dictum, sed maximus nisi porttitor. Aliquam sollicitudin pretium orci quis rhoncus. In in ipsum est. Duis quis lectus consequat, aliquet ligula consequat, aliquet nulla. Pellentesque pulvinar auctor erat, et mattis odio tristique eget. vehicula sapien suscipit ac.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="abt_vdo">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="http://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="slantdiv1 hidden-xs"></div>
    </section>

    <section class="whyus_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading">
                        <h5>WHY US</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="whybox">
                        <div class="why_icon_div">
                            <img src="{{asset('frontend/images/a1.png')}}" alt="" class="img-responsive">
                        </div>
                        <div class="heading">
                            <h4>Emotional life<br>Insurance</h4>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit uspendisse tristique vehces rhoncus turpis vitae tincidunt ed venenatis </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="whybox">
                        <div class="why_icon_div">
                            <img src="{{asset('frontend/images/a2.png')}}" alt="" class="img-responsive">
                        </div>
                        <div class="heading">
                            <h4>Guide your loved ones <br>forever</h4>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit uspendisse tristique vehces rhoncus turpis vitae tincidunt ed venenatis </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="whybox why_last">
                        <div class="why_icon_div">
                            <img src="{{asset('frontend/images/a3.png')}}" alt="" class="img-responsive">
                        </div>
                        <div class="heading">
                            <h4>Store your <br>passwords</h4>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit uspendisse tristique vehces rhoncus turpis vitae tincidunt ed venenatis </p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="slantdiv4 hidden-xs"></div>
    </section>

    <section class="quote_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="quote_box">
                        <p>“Donec eget ornare nibh tiam laoreet enim id volutpat gravida uspendis otenti nteger malesuada tristique fauci bus ed in libero eget neque ”</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>

@endsection