<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function pages()
    {
        $pages=Page::all();
        return view('admin.pages',['pages'=>$pages]);
    }

    public function addPage()
    {
        $pages=Page::all();
        return view('admin.addpage',['pages'=>$pages]);

    }

    /**
     * @param Request $request
     */
    public function storePage(Request $request)
    {
       // dd($request);
        $this->validate($request,[
            'page_name'=>'required',
            'title'=>'required',
            'slug'=>'required',
            'meta_title'=>'required',
            'meta_keyword'=>'required',
            'meta_description'=>'required'
        ]);

        if($request->file('banner_image') != ""){
            $image = $request->file('banner_image');
            $banner_image = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = 'images/cms';
            $image->move($destinationPath, $banner_image);
        } else {
            $banner_image = "";
        }

        Page::create([
            'name'=>$request->page_name,
            'title'=>$request->title,
            'slug'=>$request->slug,
            'banner_image'=>$banner_image,
            'meta_title'=>$request->meta_title,
            'meta_keyword'=>$request->meta_keyword,
            'meta_description'=>$request->meta_description,
            'content'=>$request->page_content,
            'status'=>1,
        ]);

        return redirect(route('admin.pages'))->with('message', 'Page added Successfully.');
    }

    public function viewPage($id)
    {
        $page=Page::find($id);
        dd($page);
    }

    public function editPage($id)
    {
        $page=Page::find($id);
        $pages=Page::all();
        return view('admin.editpage',['page'=>$page,'pages'=>$pages]);
    }

    public function updatePage(Request $request)
    {

        if($request->file('banner_image')!=""){
            $image = $request->file('banner_image');
            $banner_image = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = 'images/cms';
            $image->move($destinationPath, $banner_image);
        } else {
            $banner_image = $request['hiddenBannerImage'];
        }

        $page = Page::find($request->page_id);

        $page->name=$request->page_name;
        $page->title=$request->title;
        $page->slug=$request->slug;
        $page->banner_image=$banner_image;
        $page->meta_title=$request->meta_title;
        $page->meta_keyword=$request->meta_keyword;
        $page->meta_description=$request->meta_description;
        $page->content=$request->page_content;
        $page->save();

        return redirect(route('admin.pages'))->with('message', 'Page Updated Successfully.');

    }

    public function getPage($slug = null)
    {
        $page = Page::where('slug', $slug)->where('active', 1);
        dd($page);
        //$page = $page->firstOrFail();

        return view($page->template)->with('page', $page);
    }
}
