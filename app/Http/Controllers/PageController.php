<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function getPage($slug = null)
    {
        $pages=Page::all();
        $pagecontent = Page::where('slug', $slug)->first();

        return view('about',['pages'=>$pages ,'pagecontent'=>$pagecontent]);
    }
}
