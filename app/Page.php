<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable=['name','title','slug','banner_image','meta_title','meta_keyword','meta_description','content','status'];
}

